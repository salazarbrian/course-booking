let logInUser = document.querySelector('#logInUser');
logInUser .addEventListener('submit', (e) => {	// let logInUser = document.querySelector('#logInUser')
	 e.preventDefault();
	 let email = document.querySelector('#email').value
	 let password = document.querySelector('#password').value

	 let body = {
	 	email : email,
	 	password : password
	 }
	fetch("http://localhost:4000/api/users/login", {
		method: "POST",
		body : JSON.stringify(body),
		headers: {
			"Content-Type" : "application/json"
		}
	}).then( res => res.json())
	.then( data => {
		console.log(data)
		if (!data) return alert("Something went wrong")
			//hiding the token
			localStorage.setItem("token",data.accessToken)
		fetch('http://localhost:4000/api/users/details',{
			headers:{
				"Authorization" : `Bearer ${localStorage["token"]}`
			}
		})
		.then( res => res.json())
		.then( user => {
			console.log(user)
			localStorage["id"] = user._id
			localStorage["isAdmin"] = user.isAdmin

			window.location.replace('./courses.html')
		})
	})
})
