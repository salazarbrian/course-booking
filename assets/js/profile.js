let token = localStorage.getItem("token")

let profileContainer = document.querySelector('#profileContainer');

const userDetailsContainer = document.querySelector('#userDetails')
const courseDetailsContainer = document.querySelector('#courseDetails')

if(!token || token === null){
	alert('You must login first!')
	window.location.href="./login.html"
} else {

	fetch('http://localhost:4000/api/users/details', {
			method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {	

			userDetailsContainer.innerHTML = `
			<div class="user-details">
			<h3><span>First Name</span>: ${data.firstName} </h3>
			<h3><span>Last Name</span>: ${data.lastName} </h3>
			<h3><span>Email</span>: ${data.email} </h3>
			<div>
			`
			console.log(data);

			data.enrollments.forEach(course => {
				fetch(
					`http://localhost:4000/api/courses/${course.courseId}`
					)
				.then(res => res.json())
				.then(data => {
					let date = new Date (course.enrolledOn)

					const tr = document.createElement('tr');
					tr.innerHTML = `
					<td>${data.name}</td>
					<td>${date}</td>
					<td>${course.status}</td>
					`;

					courseDetailsContainer.appendChild(tr)
				})
			})
		})
}