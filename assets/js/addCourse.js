let createCourse = document.querySelector('#createCourse');
createCourse.addEventListener('submit', (e) => {
	//prevent pag pasa
	 e.preventDefault();
	 let name = document.querySelector('#name').value
	 let price = document.querySelector('#price').value
	 let description = document.querySelector('#description').value


	 let body = {
	 	name,
	 	price,
	 	description
	 }

	fetch("http://localhost:4000/api/courses", {
		method: "POST",
		body : JSON.stringify(body),
		headers: {
			"Content-Type" : "application/json"
		}
	}).then( res => res.json())	
	.then( data => {
		console.log(data)
		if(data) {
			alert("Registration successful")
			window.location.replace("./courses.html")
		} else{
			alert("Registration went wrong")
		}
	})
})