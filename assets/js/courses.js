//switch.addEventListener('change')

let token = localStorage.getItem('token')

let adminUser = localStorage["isAdmin"];
let adminButton = document.querySelector('#adminButton')
let cardFooter;


let urlNotAdmin = ('http://localhost:4000/api/courses/')
let urlAdmin = ('http://localhost:4000/api/courses/all')


	if (adminUser == "false" || !adminUser) {
		fetch(urlNotAdmin)
		.then( res => {
			return res.json()
		})
		.then( data =>{
			console.log(data)

			function displayCardFooter(courseId){
				console.log(courseId)
				if (adminUser == "false" || !adminUser) {
					cardFooter = `<a href="./course.html?courseId=${courseId}" class="btn btn-primary">Select Course</a>`
				} 
				return cardFooter
			}

			let courseContainer = document.querySelector('#courseContainer');
			let courseData = data.map( elem => {
				return `
					<div class="col-md-6 my-3">
						<div class="card">
						    <div class="card-body">
					 	        <h5 class="card-title">${elem.name}</h5>
						        <p class="card-text text-right">&#8369; ${elem.price}</p>
						        <p class="card-text">${elem.description}</p>
						    </div>
						    <div class="card-footer">
						    	${displayCardFooter(elem._id)}
						    </div>
					    </div>
					</div>
				`
			})

			courseContainer.innerHTML = courseData.join("");


		})
}else{
		fetch(urlAdmin)
		.then( res => {
			return res.json()
		})
		.then( data =>{
			console.log(data)

				function displayCardFooter(courseId){

					if (adminUser == "true" || adminUser) {


					cardFooter = `
						<a 
							href="./editCourse.html?courseId=${courseId}" 
							class="btn btn-primary editButton"
						>
							Edit
						</a>
			    		<a 
				        	href="./enrollees.html?courseId=${courseId}" 
				        	class="btn btn-warning enrolleesButton"
			        	>
			        		Enrollees
			    		</a>
			    		<a 
				        	href="./addCourse.html?courseId=${courseId}" 
				        	class="btn btn-danger deleteButton"
			        	>
			        		Add Course
			    		</a>

			    		`
				}
				return cardFooter
			}

			let courseContainer = document.querySelector('#courseContainer');
			let courseData = data.map( elem => {
				return `
					<div class="col-md-6 my-3">
						<div class="card">
						    <div class="card-body">
					 	        <h5 class="card-title">${elem.name}</h5>
						        <p class="card-text text-right">&#8369; ${elem.price}</p>
						        <p class="card-text">${elem.description}</p>
						    </div>
						    <div class="card-footer">
						    	${displayCardFooter(elem._id)}
						    </div>
					    </div>
					</div>
				`
			})

			courseContainer.innerHTML = courseData.join("");
})

		}


