let registerForm = document.querySelector('#registerForm');
registerForm.addEventListener('submit', (e) => {
	//prevent pag pasa
	 e.preventDefault();
	 let firstName = document.querySelector('#firstName').value
	 let lastName = document.querySelector('#lastName').value
	 let email = document.querySelector('#email').value
	 let mobileNo = document.querySelector('#mobileNo').value
	 let password = document.querySelector('#password').value
	 let confirmPassword = document.querySelector('#confirmPassword').value

	 let body = {
	 	firstName,
	 	lastName,
	 	email,
	 	mobileNo,
	 	password,
	 	confirmPassword
	 }

	//default kasi si get! kaya yung pangalawang method pwedeng
	fetch("http://localhost:4000/api/users/", {
		method: "POST",
		body : JSON.stringify(body),
		headers: {
			"Content-Type" : "application/json"
		}
	}).then( res => res.json())
	.then( data => {
		console.log(data)
		if(data) {
			alert("Registration successful")
			//window.location.replace - ililipat sa next page
			window.location.replace("./login.html")
		} else{
			alert("Registration went wrong")
		}
	})
})