const urlParams = new URLSearchParams(window.location.search);
const courseId = urlParams.get('courseId');

console.log(courseId);
let url = `http://localhost:4000/api/courses/${courseId}`;

fetch(url)
.then(res => res.json())
.then( data =>{

	let courseName = document.querySelector('#courseName')
	let courseDesc = document.querySelector('#courseDesc')
	let coursePrice = document.querySelector('#coursePrice')
	courseName.innerText = data.name
	courseDesc.innerText = data.description
	coursePrice.innerText = data.price

	document.querySelector("#enrollButton").addEventListener(`click`, () => {
		fetch(`http://localhost:4000/api/users/enroll`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage["token"]}`
			},
			body: JSON.stringify({
				courseId : courseId
			})
		}).then( res => res.json())
		.then( data => {
			console.log(data)
			if (data === true){
				alert("Enrollment Success!")
				window.location.replace('./courses.html')
			}else{
				alert("Something went wrong!")
			}
		})
	})
})
