let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem('token')

let courseId = params.get('courseId');

let name = document.querySelector("#courseName");
let price = document.querySelector("#coursePrice");
let description = document.querySelector("#courseDescription");

fetch(`http://localhost:4000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data)

	//assign the current values to the inputs fields
	name.value = data.name
	price.value = data.price
	description.value = data.description

	document.querySelector("#editCourse").addEventListener("submit", (e) => {
		e.preventDefault()

		let courseName = name.value
		let courseDesc = description.value
		let coursePrice = price.value
		let token = localStorage.getItem('token')

		fetch('http://localhost:4000/api/courses', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				_id: courseId,
				name: courseName,
				description: courseDesc,
				price: coursePrice
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data === true) {
				window.location.replace('./courses.html')
			} else {
				alert("something went wrong!")
			}

		})

	})
	

		document.querySelector('#form-select').addEventListener('change', (e) => {

			if(e.target.value == "true"){
				fetch(`http://localhost:4000/api/courses/isTrue/${courseId}`, {
						method: 'DELETE',
						headers: {
							'Authorization': `Bearer ${token}`
						}
					})
					.then(res => res.json())
					.then(data => {
						console.log(data)
					})
			}
			 if(e.target.value == "false"){
				fetch(`http://localhost:4000/api/courses/isFalse/${courseId}`, {
					method: 'DELETE',
					headers: {
						'Authorization': `Bearer ${token}`
					}
				})
				.then( res => res.json())
				.then( (data) => console.log(data))
			
			}
		})
})
