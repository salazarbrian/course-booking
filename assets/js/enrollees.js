let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem("token");
let courseId = params.get('courseId');
let profileContainer = document.querySelector('#profileContainer');

if(!token || token === null){
	alert('You must login first!')
	window.location.href="./login.html"
} else {
	fetch(`http://localhost:4000/api/courses/${courseId}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		console.log(data)
		let enrollees = data.enrollees
		console.log(enrollees)
		// console.log(enrolledCourses.length === 0)
		let message = ""; //we will add message if the enrolled courses is empty, else it will stay empty string

		if(enrollees.length === 0){
			message = "No one enrolled yet!."
		} else {
			let listOfCourses = enrollees.map(course =>{
				let d = new Date(course.enrolledOn)
				console.log(d)
				return `
					<tr>
		                <td>${course.enrolledOn}</td>
		                <td>${course.userId}</td>
		            </tr>
	            `
			})
		 console.log(listOfCourses)

			message = listOfCourses.join("")
		}

		const profileDetails = 
			` <div class="col-12">
				        <section class="my-5">
				          <table class="table table-hover">
				            <thead>
				              <tr>
				                <th> User ID</th>
				                <th> Enrolled On</th>
				              </tr>
				            </thead>
				            <tbody>
				              ${message}
				            </tbody>

				          </table>
				        </section>
				      </div>
				      `

		// console.log(profileDetails)
		profileContainer.innerHTML = profileDetails;

	})
}
